//Crud Operations

/*
	- CRUD operations are the heart of any backend application
	- Mastering the CRUD operations is essential for any developer
	- Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information
*/

//to switch dbs, we use - use <dbName>

//[Create] Insert documents
	//Insert one document
		//Syntax
			//db.collectionName.insertOne({object});

	db.users.insertOne({
		"firstName":"John",
		"lastName":"Smith",
	})

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone: "87000",
			email:"janedoe@gmail.com"
		},
		courses:["CSS","JavaScript","Python"],
		department: "none"
	})

	//Insert many
	/*
		Syntax
			db.collectionName.insertMany([{objectA},{objectB}])
	*/

	db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact:{
			phone: 87000,
			email: "stephenhawking@gmail.com"
		},
		courses:["Python","React","PHP"],
		department: "none"},
		{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact:{
			phone: 87002,
			email: "neilarmstrong@gmail.com"
		},
		courses:["React","Laravel","Sass"],
		department: "none"


	}
	])

//[Read] Finding Documents

//Find
	/*
		Syntax:
		db.collectionName.find();
		db.collectionName({})
	*/

//Finding a single document
	//leaving the search criteria empty will tetrieve ALL the documents
	db.users.find();

	db.users.find({firstName:"Stephen"})

//Finding document with multiple parameters
	/*
		Syntax
			db.collectionName.find({fieldA: valueA, fieldB: valueB)
	*/

	db.users.find({lastName:"Armstrong",age:82})

//[Update] Updating a document

//Updating a single Document

	//let's add another document

	db.users.insertOne({
		firstNAme: "Test",
		lastName: "Test",
		age: 0,
		contact:{
			phone: "00000",
			email:"test@gmail.com"
		},
		courses: [],
		department: "none"
	})

	/*
		Syntax:
			db.collectionName.updateOne({criteria},{$set:{field,value}});
	*/

	//set operator replaces the value of a field with the specified value
	db.users.updateOne(
		{firstName: "Test"},
		{ $set:{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact:{
				phone: "12345678",
				email:"billgates@gmail.com"
			},
			courses:["PHP","Laravel","THML"],
			department:"Operations",
			status: "Active"
			}
		}

	)
	

	db.users.find({firstName:"Bill"})
	//status is inserted automatically

//Update multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria},{$set:{field,value}})
*/

	db.users.updateMany(
		{department: "none"},

		{
			$set:{department:"HR"}
		}
	)



//Delete

//Create document to delete

	db.users.insert({
		firstName: "test"
	})

//Deleting a single document
	/*
		Syntax
		.collectionName({criteria})
	*/

	db.users.deleteOne({
		firstName: "test"
	})

//Delete MAny

	/*
		Be careful when using the deleteAmny method. IF we do not add a search criteria, it will DELETE ALL DOCUMENTS in a db
		DO NOT USE: databaseName.collectionName.deleteMany()
		Syntax:
		db.collectionNamedeleteMany({criteria})
	*/

	db.users.deleteMany({
		firstName: "Bill"
	})