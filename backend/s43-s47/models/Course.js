//Dependencies
const mongoose = require("mongoose");

//Schema/Blueprint

const courseSchema = new mongoose.Schema({

	name: {
		type : String,
		required: [true,"Course is required"]
	},	
	description: {
		type : String,
		required: [true,"Description is required"]
	},
	price: {
		type : String,
		required: [true,"Price is required"]
	},
	isActive: {
		type : Boolean,
		required: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]

})

//Model
module.exports = mongoose.model("Course",courseSchema);